package com.usm.recipes.model;

import javax.persistence.*;

@Entity
@Table(name = "T_Recipe")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Lob
    @Column( columnDefinition ="TEXT",name = "description")
    private String description;

    @Column(name = "title")
    private String title;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User creator;

    public Recipe(String description, String title, User creator) {
        this.description = description;
        this.title=title;
        this.creator = creator;
    }
    public Recipe(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle(){return title;}

    public void setTitle(String title){this.title=title;}

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }
}
