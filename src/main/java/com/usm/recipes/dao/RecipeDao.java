package com.usm.recipes.dao;

import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeDao extends JpaRepository<Recipe, Long> {

   List<Recipe> getAllRecipesByCreator(User creator);
}
