package com.usm.recipes.Service.impl;
import com.usm.recipes.Service.EmailValidator;
import org.springframework.stereotype.Service;

@Service("emailValidator")
public class EmailValidatorImpl implements EmailValidator {
    @Override
    public boolean isValidEmail(String email) {
        return org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(email);
    }
}
