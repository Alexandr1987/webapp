package com.usm.recipes.service.impl;

import com.usm.recipes.dao.UserDao;
import com.usm.recipes.model.User;
import com.usm.recipes.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    @Transactional
    public void save(User user) {
              userDao.save(user);
    }

    @Override
    public void update(User user) {
        userDao.save(user);

    }

    @Override
    public void delete(User userId) {

        userDao.delete(userId);
    }


    @Override
    public Long getUser(Long userId) {

        userDao.findById(userId);
        return userId;
    }

    @Override
    public List getAllUser() {

        return (List) userDao.findAll();
    }
}
