package com.usm.recipes.service;

import com.usm.recipes.model.Recipe;

import java.util.List;

public interface RecipeService {
    public void save(Recipe recipe);
    public void update(Recipe recipe);
    public void delete(Recipe recipeId);

    public Long getRecipe(Long recipeId);
    public List getRecipeById();
}
