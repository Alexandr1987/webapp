package com.usm.recipes.controller;

import com.usm.recipes.Service.RecipeService;
import com.usm.recipes.Service.UserService;
import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class RecipeController {
@Autowired
    private RecipeService recipeService;
@Autowired
    private UserService userService;
    @GetMapping("/create-recipe")
    public String enterRecipe(Model model){
        model.addAttribute("recipe",new Recipe());
        return "create-recipe";
    }

    @PostMapping("/create-recipe")
    public String registerRecipe(@ModelAttribute("recipe") Recipe recipe, Principal principal, Model model){
        User user = userService.getUsersByEmail(principal.getName());
        recipe.setCreator(user);
        recipeService.saveOrUpdate(recipe);
        model.addAttribute("user",user);
        model.addAttribute("recipes", recipeService.getAllRecipes());
        return "recipe";
    }
}
