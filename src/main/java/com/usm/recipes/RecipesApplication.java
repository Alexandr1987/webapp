package com.usm.recipes;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.usm.recipes.dao", enableDefaultTransactions = false)
@SpringBootApplication
@ComponentScan(basePackages = ("com.usm.recipes"))
public class RecipesApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(RecipesApplication.class, args);
    }

    @Override
    public void run(String... args) {
    }
}

