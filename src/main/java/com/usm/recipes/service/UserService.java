package com.usm.recipes.service;

import com.usm.recipes.model.User;

import java.util.List;

public interface UserService {
    public void save(User user);
    public void update(User user);
    public void delete (User userId);

    public Long getUser(Long userId );
    public List getAllUser();

}
