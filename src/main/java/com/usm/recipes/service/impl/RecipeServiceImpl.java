package com.usm.recipes.service.impl;

import com.usm.recipes.dao.RecipeDao;
import com.usm.recipes.model.Recipe;
import com.usm.recipes.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecipeServiceImpl implements RecipeService {

    @Autowired
    private RecipeDao recipeDao;


    @Override
    public void save(Recipe recipe) {
        recipeDao.save(recipe);
    }

    @Override
    public void update(Recipe recipe) {
        recipeDao.save(recipe);
    }

    @Override
    public void delete(Recipe recipeId) {
        recipeDao.delete(recipeId);
    }

    @Override
    public Long getRecipe(Long recipeId) {
        recipeDao.findById(recipeId);
        return recipeId;
    }

    @Override
    public List getRecipeById() {
        return (List)recipeDao.findAll();
    }
}
