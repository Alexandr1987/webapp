package com.usm.recipes;

import com.usm.recipes.dao.RecipeDao;
import com.usm.recipes.dao.UserDao;
import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RecipesApplicationTest {

    @Autowired
    UserDao userDao;
    @Autowired
    RecipeDao recipeDao;

    @Test
    @Transactional
    @Rollback(true)
    public void testAddUser(){
        User user= new User("new@email.com","12345","Vasea"
                ,"Pulin","069111555");
        userDao.save(user);
        userDao.delete(user);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void testAddRecipe(){
        User user = new User("old@email.com","654321","Petea"
                ,"Ivanov","069654321");

        Recipe recipe= new Recipe();
        recipe.setId(1L);
        recipe.setCreator(user);
        recipe.setEmail("old@email.com");
        recipe.setDescription("New Recipe");

        recipeDao.save(recipe);
        recipeDao.delete(recipe);
        recipeDao.findById(1L);
    }
}